#!/usr/bin/env python3

import csv
import sys
from urllib.parse import urlparse
import re

domains= {}

def domainduplicatecheck (domain):
    ret = True
    if domain and domain is not None:
        try:
            if domains[domain]:
                ret = True
        except:
            domains[domain] = 1
            ret = False
    return(ret)

def printdomain(domain, row):
    bundesland = row['ID'][0:1]
    print( domain + ",City,Non-Federal Agency," +  row['NAME'].strip() + " (" + row['ID'] + ")," + bundesland )


try:
    if sys.argv[2] and sys.argv[2] is not None:
        encoding = sys.argv[2]
except:
    encoding = 'utf-8'

with open(sys.argv[1], encoding = encoding ) as csvfile:
    reader = csv.DictReader(csvfile, fieldnames=['ID','NAME','MAIL'], delimiter=",")
    for row in reader:
        mail = row['MAIL']
        if mail and mail is not None:
            domain = mail.split('@')[1].strip()
            if not domainduplicatecheck(domain):
                printdomain(domain, row)

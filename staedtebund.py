#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
from bs4 import SoupStrainer

baseurl='https://www.staedtebund.gv.at/organisation/oesterr-staedtebund/gvat-gemeindenamen/'
bundesland = [ "burgenland", "niederoesterreich", "kaernten", "oberoesterreich", "salzburg", "steiermark", "tirol", "vorarlberg", "wien" ]

contenttable = SoupStrainer("table", class_="contenttable")
# <table class="contenttable">

for bl in bundesland:
    url = baseurl + bl + "/"
    response = requests.get(url)
    table = BeautifulSoup(response.text, 'html.parser', parse_only=contenttable)
    rows = table.find_all('tr')
    for tr in rows:
        td = tr.find_all('td')
        if not td or len(td)<4 or td[3].text.strip() == '':
            continue
        else:
            print(td[3].text + ",City,Non-Federal Agency," + td[0].text + " (" + td[2].text + ")," + td[2].text[0:1] )

.DEFAULT_GOAL := all
.PHONY : all
.PHONY : clean
.PHONY : clean-sources

HEADER = Domain Name,Domain Type,Agency,City,State

burgenland:
	curl -s https://data.bgld.gv.at/files/LAD-Informationstechnologie/verwaltungsadressen_burgenland.csv -o data/source/burgenland_verwaltungsadressen.csv
	# check for and remove BOM
	if (file data/source/burgenland_verwaltungsadressen.csv | grep -q 'with BOM'); then \
		sed -i '1s/^\xEF\xBB\xBF//' data/source/burgenland_verwaltungsadressen.csv; \
	fi 
	python ./verwaltungsadressen.py data/source/burgenland_verwaltungsadressen.csv 'utf-8' > data/source/burgenland.csv

niederoesterreich:
	curl -s https://open-data.noe.gv.at/ogd-data/LAD1-IT/verwaltungsadressen_noe.csv -o data/source/noe_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/noe_verwaltungsadressen.csv > data/source/niederoesterreich.csv

oberoesterreich:
	curl -s https://www.land-oberoesterreich.gv.at/files/ogd/dbexport/verwaltungsadressen_ooe.csv -o data/source/ooe_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/ooe_verwaltungsadressen.csv > data/source/oberoesterreich.csv

kaernten:
	curl -s https://gis.ktn.gv.at/webgisapi/datalinq/select/gemktn@gem-id | jq -r '.[] | "\(.GKZ),\(.GEMEINDENAME),\(.EMAIL)"' >data/source/ktn_gemeindeaemter.csv
	python gemeindeaemter.py data/source/ktn_gemeindeaemter.csv >data/source/kaernten_work.csv
	curl --insecure -s https://www.ktn.gv.at/Verwaltung/Gemeinden/Gemeindeliste?export=1 -o data/source/kaernten_gl.csv
	python kaertnen-gemeindeadressen.py data/source/kaernten_gl.csv >data/source/kaernten_work2.csv
	zsh -c "sort --unique --ignore-case --field-separator=, --key=1,1 --output=data/source/kaernten.csv data/source/kaernten_work.csv data/source/kaernten_work2.csv"

salzburg:
	curl -s https://www.salzburg.gv.at/ogd/d49a127d-f60f-4d6f-bb1c-46d70ff14699/verwaltungsadressen_sbg.csv -o data/source/salzburg_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/salzburg_verwaltungsadressen.csv > data/source/salzburg.csv

steiermark:
	curl -s https://egov.stmk.gv.at/at.gv.stmk.ogd-p/verwaltung_und_politik/verwaltungsadressen_stmk.csv -o data/source/stmk_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/stmk_verwaltungsadressen.csv > data/source/steiermark_work.csv
	curl -s https://egov.stmk.gv.at/at.gv.stmk.ogd-p/verwaltung_und_politik/gem -o data/source/stmk_GemeindeKontaktdaten.xml
	python gemeindekontaktdaten.py data/source/stmk_GemeindeKontaktdaten.xml >> data/source/steiermark_work.csv
	zsh -c "sort --unique --ignore-case --field-separator=, --key=1,1 --output=data/source/steiermark.csv data/source/steiermark_work.csv"

tirol:
	curl -s https://gis.tirol.gv.at/ogd/verwaltung_politik/verwaltungsadressen_tirol.csv -o data/source/tirol_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/tirol_verwaltungsadressen.csv > data/source/tirol.csv

vorarlberg:
	# sic. an http link.
	curl -s http://data.vorarlberg.gv.at/Katalog/Verwaltung/verwaltungsadressen_vorarlberg.csv -o data/source/vorarlberg_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/vorarlberg_verwaltungsadressen.csv > data/source/vorarlberg.csv

wien:
	curl -s https://www.wien.gv.at/data/csv/verwaltungsadressen.csv -o data/source/wien_verwaltungsadressen.csv
	python ./verwaltungsadressen.py data/source/wien_verwaltungsadressen.csv > data/source/wien.csv

staedtebund:
	python ./staedtebund.py >data/source/staedtebund.csv

gemeinden: burgenland niederoesterreich oberoesterreich kaernten tirol vorarlberg salzburg steiermark wien staedtebund
	./grab_gemeindeliste.bash
	zsh -c "sort --unique --ignore-case --field-separator=',' --output=data/domains.cities.csv data/source/{burgenland,kaernten,niederoesterreich,oberoesterreich,salzburg,steiermark,tirol,vorarlberg}.csv data/source/staedtebund.csv"
	
clean-sources:
	rm -f data/source/*.zip
	rm -f data/source/*.csv
	rm -f data/source/*.dbf
	rm -f data/*.csv

clean: clean-sources
	rm -f data/domains*

all: gemeinden
	zsh -c "sort --unique --ignore-case --field-separator=',' --output=data/domains.csv data/domains.cities.csv"


#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import sys
from urllib.parse import urlparse
import re

domains = {}

def domainduplicatecheck (domain):
    ret = True
    if domain and domain is not None:
        try:
            if domains[domain]:
                ret = True
        except:
            domains[domain] = 1
            ret = False
    return(ret)

def printdomain(domain, gemnr, name):
    bundesland = gemnr[0:1]
    bez = name.strip() + " (" + gemnr + ")"
    print( domain + ",City,Non-Federal Agency," +  bez.strip() + "," + bundesland )


tree = ET.parse(sys.argv[1])
root = tree.getroot()

for gemeinde in root.findall('Gemeinde'):
    homepage = gemeinde.find('webadr').text
    mail = gemeinde.find('email').text
    name = gemeinde.find('gemname').text
    gemnr = gemeinde.find('gemnr').text
    oedomain = gemeinde.find('oedomain').text

    if homepage and homepage is not None:
        domain = urlparse(homepage).netloc.strip()
        # strip www
        domain = re.sub('.*w\.', '', str(domain), 1)
        if not domainduplicatecheck(domain):
            printdomain(domain, gemnr, name)
    if mail and mail is not None:
        domain = mail.split('@')[1].strip()
        if not domainduplicatecheck(domain):
            printdomain(domain, gemnr, name)
    if oedomain and oedomain is not None:
        domain = oedomain
        # strip www
        domain = re.sub('.*w\.', '', str(domain), 1)
        if not domainduplicatecheck(domain):
            printdomain(domain, gemnr, name)

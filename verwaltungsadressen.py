#!/usr/bin/env python3

import csv
import sys
from urllib.parse import urlparse
import re

domains= {}

okz = "OKZ"
bez1 = "Bezeichnung 1"
bez2 = "Bezeichnung 2"
bez3 = "Bezeichnung 3"
hp = "Homepage"
email = "Mail"

def domainduplicatecheck (domain):
    ret = True
    if domain and domain is not None:
        try:
            if domains[domain]:
                ret = True
        except:
            domains[domain] = 1
            ret = False
    return(ret)

def printdomain(domain, row):
    bundesland = row[okz][1:2]
    # Vorarlberg adds more text for Gemeinden
    if not bundesland.isnumeric():
        bundesland = row[okz][4:5]
    name =  row[bez1] + " " + row[bez2] + " " + row[bez3]
    print( domain + ",City,Non-Federal Agency," +  name.strip() + "," + bundesland )


try:
    if sys.argv[2] and sys.argv[2] is not None:
        encoding = sys.argv[2]
except:
    encoding = 'latin'

with open(sys.argv[1], encoding = encoding ) as csvfile:
    reader = csv.DictReader(csvfile, delimiter=";")
    for row in reader:
        try:
            homepage = row[hp]
        except:
            bez1 = bez1.upper().replace(" ", "")
            bez2 = bez2.upper().replace(" ", "")
            bez3 = bez3.upper().replace(" ", "")
            hp = hp.upper()
            email = email.upper()
            homepage = row[hp]

        if homepage and homepage is not None:
            domain = urlparse(homepage).netloc.strip()
            # strip www
            domain = re.sub('.*w\.', '', str(domain), 1)
            if not domainduplicatecheck(domain):
                printdomain(domain, row)
        mail = row[email]
        if mail and mail is not None:
            domain = mail.split('@')[1].strip()
            if not domainduplicatecheck(domain):
                printdomain(domain, row)

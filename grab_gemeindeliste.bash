#!/usr/bin/env bash
#
# AUTHORS, LICENSE and DOCUMENTATION
#
# Uses the perl tool (dbf_dump) that allows to convert dbf files (dBase) to csv (comma-separated values).
# sudo aptitude install libdbd-xbase-perl

#
set -eu -o pipefail

ZIPFILE=$(curl -s https://www.data.gv.at/katalog/api/3/action/package_show\?id\=566c99be-b436-365e-af4f-27be6c536358 | jq -r '.result.resources[0].url')
echo "$ZIPFILE"
rm -f data/source/gemeinden.zip
rm -f data/source/*.dbf
curl -s "$ZIPFILE" -o data/source/gemeinden.zip
unzip data/source/gemeinden.zip \*.dbf -d data/source
dbf_dump --fs="," data/source/*.dbf >data/source/gemeinden.csv


